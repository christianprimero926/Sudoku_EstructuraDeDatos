#ifndef SUDOKU_H
#define SUDOKU_H

#include <iostream>


class sudoku{
    public:
        sudoku();
        void insert_matrix(int i, int j, int val);
        int read_matrix(int i, int j);
        void init_matrix(int i, int j, int val);
        bool is_posible_row(int row, int value);
        bool is_posible_colum(int column, int value);
        bool is_posible_region(int row, int column, int value);
        bool you_win();
    private:
        int matrix[9][9];
        int valor[9][9];
        int row[10];
        int col[10];
        int block[5][5];

};

#endif // SUDOKU_H
