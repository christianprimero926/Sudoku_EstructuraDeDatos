#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <cstdlib>

#include <stack>
#include <QDebug>

#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <errno.h>

#include "sudoku.h"

using namespace std;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:


    void on_tablero_cellClicked(int row, int column);

    void click__on_pb(int num);

    void on_loadBoard_clicked();

    void on_pushButton_1_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_6_clicked();

    void on_pushButton_7_clicked();

    void on_pushButton_8_clicked();

    void on_pushButton_9_clicked();

    void on_startButton_clicked();

    void on_pushButton_sugerencia_clicked();

    void on_historial_clicked();

    void on_deshacer_clicked();

    void on_rehacer_clicked();

private:
    Ui::MainWindow *ui;
    int Column, Row;
    QString filename;
    QString filenameHistorial = "historial";
    stack<int> undo;
    stack<int> redo;
};

#endif // MAINWINDOW_H
