#include "mainwindow.h"
#include "ui_mainwindow.h"

using namespace std;

//Objeto de sudoku
sudoku* matx = new sudoku();

//Constructor y puntero, que me permite apuntar (valga la redundancia) apuntar a los elementos en la GUI.
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{    
    ui->setupUi(this);
}

//Destructor de la GUI.
MainWindow::~MainWindow()
{    
    delete ui;
}

//Funcion que me permite saber en que posicion (fila, columna) estoy en la tabla o tablero.
void MainWindow::on_tablero_cellClicked(int row, int column)
{
    //Fila de la tabla, este se actualiza ya que la funcion se llama cada vez que selecciono una casilla.
    Row = row;
    //Columna de la tabla.
    Column = column;

    //Creamos un string y en el concatenamos la fila y columna para que nos quedue en un formato (0,0).
    QString str = "";
    str += '0' + row;
    str += ',';
    str += '0' + column;
    const QString cstr = str;

    //Se muestra en un label posicion el string ya concatenado.
    ui->posicion->setText(cstr);

    /**Este es un if que nos ayuda al momento de usar la funcion deshacer,
     * lo que hace es decirnos que si en la posicion en la que me encuentro
     * el valor es igual a 0, agreguelo a la pila, esto nos ayuda, ya que
     * al momento de deshacer no nos mostraba hasta este valor cero, ya que nunca
     * llegaba a guardarse en la fila.
     *
     * El defecto de este, es que si seleccionamos muchas casillas con el valor cero,
     * estas se guardaran en la pila, y pareceria que no estamos deshaciendo, pero en
     * realidad deshacemos la cantidad de 0 que hayamos seleccionado.
    **/
    if(matx->read_matrix(Row, Column) == 0){
        undo.push(Row);
        undo.push(Column);
        undo.push(0);
    }

}

//Funcion para cargar el tablero de un archivo en el PC.
void MainWindow::on_loadBoard_clicked()
{
    //Se destruye previamente el historial de jugadas cada vez que se carga un nuevo tablero.
    QFile archivo(filenameHistorial);
    archivo.remove();

    /***
     * me permite seleccionar el archivo .txt con el
     * tablero guardado
     * se recomienda cargar un tablero funcional, debido a que por
     * cuestiones de tiempo no se implementaron las validaciones
     * necesarias.
    **/
    filename = QFileDialog::getOpenFileName(
                this,
                tr("Open File"),
                "C://",
                "Text Files(*.txt)"
                );
    //Se guarda el archivo en una variable de tipo QFile.
    QFile file(filename);
    //Se pregunta si se encuentra el archivo de lo contrario nos devolvera un mensaje en consola.
    if(! file.open(QIODevice::ReadOnly | QIODevice::Text)){
      qDebug()<<"No se encuentra el archivo de texto!";
    }

    QString todoeltexto;
    //Creamos la variable que contiene todo el archivo.
    todoeltexto.append(file.readAll());//el metodo readAll ejeculalo solo una vez por función.
//    qDebug() << "Contenido del Texto: " << todoeltexto;

    //Creamos una variable de Lista partiendo la variable todoeltexto saltos de linea.
    QStringList listadeColumnas = todoeltexto.split("\n");
    //Seteamos la cantidad de columnas segun la cantidades de \n.

    ui->tablero->setColumnCount(listadeColumnas.size()-1);
//    qDebug()<<"Cantidad de columnas: "<<listadeColumnas.size();

    //Iniciamos Bucle for
    for(int i=0;i<9;i++){

        //Creamos la variable que contiene los datos de una columna.
        QString todaunaColumna;

        //Añadimos a una columna sus datos
        todaunaColumna.append(listadeColumnas.at(i));

        //Creamos una variable de Lista partiendo la variable todaunacolumna por el signo |.
        QStringList listadeceldas  = todaunaColumna.split("|");
//        qDebug()<<"Fila Numero "<< i << listadeColumnas.at(i);

        for(int j=0;j<9;j++){
//            qDebug()<<"Dato de Fila Numero "<< j << " " << listadeceldas.at(j);

            //Creamos una dato para cada celda.
            QByteArray datodecelda;            

            //Incluimos el dato en la variable de celda.            
            datodecelda.append(listadeceldas.at(j));

            /**Transformamos el valor de de la variable de celda de un QByteArray a un int,
             * para que nos quede mas facil manipular el valor en la matriz.
            **/
            bool ok;
            int valorint = datodecelda.toInt(&ok);

            //Luego llamamos al metodo insert_matriz para ingresar los valores en la matriz.
            matx->insert_matrix(i,j,valorint);

            //Iniciamos las matrices y vectores que nos ayudaran a validar el juego.
            matx->init_matrix(i,j,valorint);
        }
    }
    /***
     * Incializamos el for para mostrar los valores de la matriz en nuestra interfaz
    **/
    for(int i=0; i<9; i++){
        for(int j=0; j<9; j++){
            QTableWidgetItem *pCell = ui->tablero->item(i,j);

            QString str = "";
            str += '0' + matx->read_matrix(i, j);
            const QString cstr = str;
            pCell->setText(cstr);

            /***Validacion que se hizo para que si hay un valor previamente cargado en el tablero,
             * no pueda ser selecionado para ser editado.
            **/
            if(matx->read_matrix(i,j)){
                pCell->setFlags(Qt::ItemIsSelectable);
            }
        }
    }
    //Luego de haber cargado el tablero, se habilita el boton para que el juego pueda comenzar.
    ui->startButton->setEnabled(true);
}

//Funcion padre para insertar el valor en la posicion fila, columna en la que este parado.
void MainWindow::click__on_pb(int num)
{
    QTableWidgetItem* Cell = ui->tablero->item(Row, Column);

    //Se llama al metodo insert_matrix para insertar el nuevo valor en la matriz.
    matx->insert_matrix(Row, Column, num);

    //se guarda la jugada en la pila, en orden de fila, columna, y el valor que se inserto.
    undo.push(Row);
    undo.push(Column);
    undo.push(num);

    //validacion para habilitar los botones deshacer o rehacer.
    if(! undo.empty()){
        ui->deshacer->setEnabled(true);
        ui->notification->setText("");
    }
    if(! redo.empty()){
        ui->rehacer->setEnabled(true);
        ui->notification->setText("");
    }

    //Se muestra el valor insertado en el tablero.
    QString str = "";
    str += '0' + matx->read_matrix(Row, Column);
    const QString cstr = str;    
    Cell->setText(cstr);

    //se crea el archivo para guardar el historial de las jugadas
    QFile archivo(filenameHistorial);
    if(archivo.open(QIODevice::Append | QIODevice::Text)){
        QTextStream datosArchivo(&archivo);

        //validacion para cuando se puede escribir en una celda del tablero
        if(!(matx->read_matrix(Row,Column) == num)){
           datosArchivo << "Este valor no puede ser modificado" << endl;
        }else{
            datosArchivo << "Se jugo en la fila ("<<Row<<") y en la columna ("<<Column<<") el numero : "<< str << endl;
        }
    }
    archivo.close();

    //validacion para cuando alguien gana el juego.
    if (matx->you_win())
    {
        ui->notification->setText("HAS GANADO!!");
        // Se almacena en el historial que ha ganado
        QFile archivo(filenameHistorial);
        if(archivo.open(QIODevice::Append | QIODevice::Text)){
            QTextStream datosArchivo(&archivo);
                datosArchivo << "Ha terminado el Juego, Has Ganado!! :) " << endl;

        }
        archivo.close();
    }
}

/***Funciones que llaman a la funcion padre y les envian el numero segun el boton que han seleccionado
 * los valores a seleccionar o ingresar estan entre 1-9.
**/
void MainWindow::on_pushButton_1_clicked()
{
    click__on_pb(1);
}
void MainWindow::on_pushButton_2_clicked()
{
    click__on_pb(2);
}
void MainWindow::on_pushButton_3_clicked()
{
    click__on_pb(3);
}
void MainWindow::on_pushButton_4_clicked()
{
    click__on_pb(4);
}
void MainWindow::on_pushButton_5_clicked()
{
    click__on_pb(5);
}
void MainWindow::on_pushButton_6_clicked()
{
    click__on_pb(6);
}
void MainWindow::on_pushButton_7_clicked()
{
    click__on_pb(7);
}
void MainWindow::on_pushButton_8_clicked()
{
    click__on_pb(8);
}
void MainWindow::on_pushButton_9_clicked()
{
    click__on_pb(9);
}

//Boton que me permite habilitar los demas botones para empezar el juego
void MainWindow::on_startButton_clicked()
{
    //Habilita el marco donde estan los numeros
    ui->tableroNum->setEnabled(true);
    //habilita el tablero
    ui->tablero->setEnabled(true);
    //habilita el boton de sugerencia
    ui->pushButton_sugerencia->setEnabled(true);

    //Si las pilas de deshacer y rehacer estan vacias deshabilita sus botones respectivamente.
    if(redo.empty()){
        ui->rehacer->setEnabled(false);
    }
    if(undo.empty()){
        ui->deshacer->setEnabled(false);
    }
    //Habilita el boton para mostrar el historial de jugadas.
    ui->historial->setEnabled(true);
}

//Funcion para generar alguna sugerencia
void MainWindow::on_pushButton_sugerencia_clicked()
{
    /***
     * este for recorre filas y columnas, y pregunta si en la posicion seleccionada,
     * que valor que no este, ni en la fila, columna o region, podemos poner,
     * se hace un triple for anidado, para este el cual el primero nos recorre las 9 filas,
     * el siguiente las columnas, y el mas interno y ultimo pregunta si el numero ya existe
     * en fila, columna o region.
     * al final las sugerencias, se almacenan en un vector temporal que se actualiza cada vez
     * que cambia de posicion y se llama esta funcion.
    **/
    int x, temp[10]={0};
    for(int i=0; i<9; i++){
        for(int j=0; j<9; j++){
            x=0;
            for(int k=1; k<=9; k++){
                if(matx->is_posible_row(Row,k) && matx->is_posible_colum(Column,k) && matx->is_posible_region(Row,Column,k)){
                    temp[x] = k;
                    x++;
                }
            }
        }
    }
    /***Concatenamos el vector que nos queda en un string para al final mostrarlo en un label si es requerido
       * por el jugador.
      **/
    QString str = "";
    for(int i = 0; i < 9; i++){
        if(temp[i] != 0){
            str += '|';
            str += '0'+temp[i];
            str += '|';
        }
    }
    const QString cstr = str;
    ui->sugerencia->setText(cstr);

    //almacenamos que el usuario ha pedido una sugerencia en el historial.
    QFile archivo(filenameHistorial);
    if(archivo.open(QIODevice::Append | QIODevice::Text)){
        QTextStream datosArchivo(&archivo);
            datosArchivo << "Se pidio una sugerencia en : ("<<Row<<", "<<Column<<") y arrojo -> "<< cstr << endl;
    }
    archivo.close();
}

//Funcion que nos muestra en un TextEdit el historial
void MainWindow::on_historial_clicked()
{
    //Buscamos el archivo
    QFile file(filenameHistorial);
    if(!file.exists()){
       qDebug() << "NO existe el archivo "<<filenameHistorial;
    }else{
       qDebug() << filenameHistorial<<" encontrado...";
    }
    QString line;

    // Lo leemos y lo mostramos
    if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
       QTextStream stream(&file);
       while (!stream.atEnd()){
           line = stream.readLine();
           ui->historialText->setText(ui->historialText->toPlainText()+line+"\n");
           qDebug() << "linea: "<<line;
       }
    }
    file.close();
}

//Funcion para deshacer una accion
void MainWindow::on_deshacer_clicked()
{
    ui->notification->setText("");
    /***
     * almacenamos en una variable lo que hay en el tope de la pila de deshacer
     * lo insertamos en la pila de rehacer, y lo eliminamos de la pila deshacer
    **/
    int num = undo.top();
    redo.push(num);
    undo.pop();
    int column = undo.top();
    redo.push(column);
    undo.pop();
    int row = undo.top();
    redo.push(row);
    undo.pop();

    //validaciones para cuando la pila de deshacer este vacia.
    if(undo.empty()){
        ui->notification->setText("No hay mas elementos para deshacer");
        ui->deshacer->setEnabled(false);
    }
    if(! redo.empty()){
        ui->rehacer->setEnabled(true);
    }

    //se muestra la accion de deshacer en el tablero
    QTableWidgetItem* Cell = ui->tablero->item(row, column);
    matx->insert_matrix(row, column, num);
    QString str = "";
    str += '0' + matx->read_matrix(row, column);
    const QString cstr = str;
    Cell->setText(cstr);

    //Se almacena en el historial la accion de deshacer
    QFile archivo(filenameHistorial);
    if(archivo.open(QIODevice::Append | QIODevice::Text)){
        QTextStream datosArchivo(&archivo);
            datosArchivo << "Se deshizo la jugada : ("<<row<<", "<<column<<") -> "<< num << endl;
    }
    archivo.close();
}

void MainWindow::on_rehacer_clicked()
{
    ui->notification->setText("");

    /***
     * almacenamos en una variable lo que hay en el tope de la pila de rehacer
     * lo insertamos en la pila de deshacer, y lo eliminamos de la pila rehacer
    **/
    int row = redo.top();
    undo.push(row);
    redo.pop();
    int column = redo.top();
    undo.push(column);
    redo.pop();
    int num = redo.top();
    undo.push(num);
    redo.pop();

    //validaciones para cuando la pila de rehacer este vacia.
    if(redo.empty()){
        ui->notification->setText("No hay mas elementos para rehacer");
        ui->rehacer->setEnabled(false);
    }
    if(! undo.empty()){
        ui->deshacer->setEnabled(true);
    }

    //se muestra la accion de rehacer en el tablero
    QTableWidgetItem* Cell = ui->tablero->item(row, column);
    matx->insert_matrix(row, column, num);
    QString str = "";
    str += '0' + matx->read_matrix(row, column);
    const QString cstr = str;
    Cell->setText(cstr);

    //Se almacena en el historial la accion de rehacer
    QFile archivo(filenameHistorial);
    if(archivo.open(QIODevice::Append | QIODevice::Text)){
        QTextStream datosArchivo(&archivo);
            datosArchivo << "Se rehizo la jugada : ("<<row<<", "<<column<<") -> "<< num << endl;
    }
    archivo.close();
}
