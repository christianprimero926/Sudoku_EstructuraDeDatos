#include "sudoku.h"

using namespace std;

sudoku::sudoku()
{

}
//Funcion para inicializar las matrices y vectores que nos ayudaran a validar
void sudoku::init_matrix(int row,int column, int val)
{
    //Se inicializa los vectores de fila y columna, y region en 0
    for (int i = 0; i < 9; ++i)
    {
        this->row[i] = 0;
        this->col[i] = 0;
        this->block[i/3][i%3] = 0;
    }
    /***Inicializamos la matriz que nos permite saber que valores fueron insertados inicialmente
     * es decir, cuando se cargo el tablero, si hay un valor en este espacio, decimos que vale 1,
     * de lo contrario valdra 0, que quiere decir que es un espacio vacio.
     * tambien cargamos los valores de los vectores fila, columna y la matriz region con un
     * desplazamiento de bits el cual se llena con el siguiente valor a la izquierda
     **/
    if(val > 0 && val <= 9){
        int sig = (1 << (matrix[row][column]-1));
        this->row[row] |= sig;
        this->col[column] |= sig;
        this->block[row/3][column%3] |= sig;
        this->valor[row][column] = 1;
    }
    else {
        this->valor[row][column] = 0;
    }
}

/***Funcion que inserta en la matriz el valor que ha seleccionado el jugador
 * segun una posicion x,y (fila,columna)
 **/
void sudoku::insert_matrix(int i, int j, int val)
{
    /***Se valida con la matriz que llenamos con 1's y 0's para saber que
     *  valores ya han sido previamente cargados en la matriz, si el valor
     * de esta matriz es uno, no podemos modificar este valor.
    **/
    if(valor[i][j] == 1){
        return;
    }else{
        this->matrix[i][j] = val;
    }
}

//Funcion que nos permite ver el valor de una celda en la matriz dada una posicion fila,columna
int sudoku::read_matrix(int i, int j)
{
    //nos retorna el valor de que se ha insertado en la celda.
    return this->matrix[i][j];
}

//Fncion que nos permite saber que valores son posibles en la fila.
bool sudoku::is_posible_row(int row, int value)
{
    for(int i=0;i<9;i++){
        if(value == matrix[row][i]){
          return false;
        }
    }
    return true;
}

//Fncion que nos permite saber que valores son posibles en la columna.
bool sudoku::is_posible_colum(int column, int value)
{
    for(int i=0;i<9;i++){
        if(value == matrix[i][column]){
          return false;
        }
    }
    return true;
}

//Fncion que nos permite saber que valores son posibles en la region.
bool sudoku::is_posible_region(int row, int column, int value)
{
    int x = (row/3)*3;
    int y = (column/3)*3;
    for(int i=0; i<3; i++){
        for(int j=0; j<3; j++){
            if(value == matrix[i+x][j+y]){
                return false;
            }
        }
    }
    return true;
}

//Funcion que nos valida el vector fila columna y la matriz region o bloque, para determinar
//si se ha completado el tablero.
bool sudoku::you_win(){
    for (int i = 0; i < 9; ++i)
    {
        row[i] = 0;
        col[i] = 0;
        block[i/3][i%3] = 0;
    }
    for (int i = 0; i < 9; ++i)
    {
        for (int j = 0; j < 9; ++j)
        {
            int sig = row[i] | col[j] | block[i/3][j/3];
            int sgn = (1 << (matrix[i][j] - 1));
            if (!(sig&sgn))
            {
                row[i] |= sgn;
                col[j] |= sgn;
                block[i/3][j/3] |= sgn;
            }
            else
            {
                return false;
            }
        }
    }
    return true;
}
